#!/bin/bash

composer install && php artisan migrate && chmod 777 -R storage && chown -R www-data:www-data storage && nohup php artisan queue:work --daemon > storage/logs/queue.log & php-fpm
