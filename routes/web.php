<?php

use Illuminate\Support\Facades\Route;

Route::fallback(function() {
    return response()->json(['error' => 'this endpoint not exist'])->setStatusCode(400);
});
