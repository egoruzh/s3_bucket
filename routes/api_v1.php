<?php

use Illuminate\Support\Facades\Route;

Route::get('/user/{id}/files', 'FileController@index')->name('files');

Route::post('/user/{id}/upload', 'FileController@store')->name('upload');

Route::get('/user/{id}/download', 'FileController@downloadFile')->name('downloadfile');

Route::get('/user/{id}/folders', 'FileController@getUserDirNamesByDir')->name('folders');

Route::get('/user/{id}/unlinkuserdir', 'FileController@unlinkUserDir')->name('unlinkuserdir');

Route::get('/user/{id}/unlink', 'FileController@unlinkUserFile')->name('unlinkUserFile');
