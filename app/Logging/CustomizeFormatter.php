<?php

namespace App\Logging;

use Illuminate\Log\Logger;
use Monolog\Formatter\LineFormatter;

class CustomizeFormatter
{
    /**
     * @var array
     */
    private $add_to_log;

    public function __construct()
    {

        $this->add_to_log =
            [
                'ip' => request()->server('REMOTE_ADDR'),
                'user_agent' => request()->server('HTTP_USER_AGENT')
            ];
    }

    /**
     * Настроить переданный экземпляр регистратора.
     *
     * @param Logger $logger
     * @return void
     */
    public function __invoke( $logger)
    {

        foreach ($logger->getHandlers() as $handler) {
            $handler->setFormatter(new LineFormatter(
                format:'[{"date": "%datetime%", "message": "%message%" },' . json_encode($this->add_to_log) . ',%context%]' . PHP_EOL,
                dateFormat:'Y-m-d H:i:s',
                allowInlineLineBreaks:true,
            ));
        }
    }
}
