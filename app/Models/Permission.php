<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Permission extends Model {
    protected $table = 'permission';

    protected $guarded = ['id'];

    public static function displayable(): array
    {
        $prepared_array = [];
        $temp = self::orderBy('module')->get()->toArray();
        foreach ($temp as $sin) {
            $prepared_array[$sin['module']][] = $sin;
        }
        return $prepared_array;
    }
    public function scopeActive($query): Model
    {
        return $query->whereStatus('1');
    }

    public function roles(): BelongsToMany
    {
        return $this->belongsToMany('App\Role');
    }
}
