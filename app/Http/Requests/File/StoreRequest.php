<?php

namespace App\Http\Requests\File;

use App\Models\S3user;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property S3user $id
 */
class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $max = env("MAX_FILE_SIZE_UPLOAD_MB", 8) * 1024;

        $mimes = env("MIME_TYPES_ENABLED", "image/jpeg,image/png,image/jpg,application/pdf,text/plain");

        return match ($this->method()) {
            'POST' => [
                'userFile' => 'required|array|min:1',
                'userFile.*' => "mimetypes:$mimes|max:$max",
                'id' => [
                    'required',
                    'integer',
                ]

            ],
            default => [
                'id' => [
                    'required',
                    'exists:s3user,id',
                    'integer',
                ]
            ]
        };
    }

    public function messages(): array
    {
        return [
            "userFile.required" => "userFile[] fields is empty",
            "userFile.array" => "The 'userFile' field must be an array.",
            "userFile.*.max" => "The :attribute must not be greater than :max kilobytes.",
            "userFile.*.mimetypes" => "The :attribute must be a file of type: :values",
            "id.exists" => "User not found",
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge(['id' => $this->route('id')]);
    }

}
