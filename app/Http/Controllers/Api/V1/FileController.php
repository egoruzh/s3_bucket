<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\File\StoreRequest;
use App\Jobs\EncryptFile;
use App\Jobs\MoveFileToS3;
use App\Models\AccessHistory;
use App\Models\Document;
use App\Models\S3user;
use Exception;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use SoareCostin\FileVault\Facades\FileVault;
use Symfony\Component\HttpFoundation\StreamedResponse;
use ZipStream;
use ZipStream\Option\Archive as ArchiveOptions;

/**
 * @property Document|Model|HasMany|object|null $document
 * @property array $writeFilenameToLog
 */
class FileController extends Controller
{
    public const FILE_DIR = '';
    public const ENABLE_REMOVE = 1;
    public const UPLOAD_STATUS = 'uploaded';
    public static string $get_param_filename = 'filename';
    public static string $encryption_extension = '.enc';
    private string $separator;
    private string $uploadClientOriginalNameFile;
    private string $writeFilename;
    private Filesystem $storage;
    private S3user $user;
    private StoreRequest $request;

    public function __construct()
    {
        $this->storage = Storage::disk(env('STORAGE_TYPE'));
        $this->separator = '/';
    }

    public function __destruct()
    {
        $to_log_request = $this->request->all();
        if (isset($this->writeFilenameToLog)) {
            $to_log_request['userFile'] = $this->writeFilenameToLog;
        }
        $history = new AccessHistory();
        $history->user_id = $this->user->id;
        $history->root = $this->getRequestDir() . '/' . $this->request->get(self::$get_param_filename);
        $history->type_access = Route::currentRouteName() ?? '';
        $history->route = Route::currentRouteAction() ?? '';
        try {
            $history->request = json_encode($to_log_request, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            Log::error(addslashes(str_replace("'", '"', $e->getMessage())), ['file' => __FILE__, 'line' => __LINE__]);
        }
        try {
            $history->save();
        } catch (Exception $e) {
            Log::error(addslashes(str_replace("'", '"', $e->getMessage())), ['file' => __FILE__, 'line' => __LINE__]);
        }
    }

    private function getRequestDir(): string
    {
        $path = $this->request->get('path');

        return rtrim(
            self::FILE_DIR
            . $this->user->id
            . (
            $path
                ? '/' . trim($path, '/')
                : ''
            ),
            $this->separator
        );
    }

    public function index(StoreRequest $request): JsonResponse
    {


        $this->request = $request;
        $this->user = $this->getUser($request);
        $folders = $this->user->getUserFolders($this->getRequestDir(), $this->request->get('recursive'));
        $files = $this->user->getUserFiles($this->getRequestDir(), $this->request->get('recursive'));
        $response = array_merge($folders, $files);
        return response()->json(
            [
                'data' => [
                    'location' => ltrim($this->getRequestDir() . '/', $this->user->id),
                    'status' => $this->folderExist(),
                    'file_count' => count($files),
                    'files' => $response,
                    'message' => $this->folderExist() ? 'success' : 'folder ' . stristr($this->getRequestDir(), '/') . ' not found.'
                ]
            ]
        );
    }

    private function getUser(StoreRequest $request): S3user
    {
        if ($this->request->method() === "POST") {
            S3user::firstOrCreate(
                ['id' => $request->id],
                ['status' => true]
            );
        }
        return S3user::firstWhere('id', $request->id);
    }

    public function unlinkUserFile(StoreRequest $request): JsonResponse
    {
        $this->request = $request;
        $this->user = $this->getUser($request);
        if (!self::ENABLE_REMOVE) {
            return response()->json(['status' => false, 'message' => 'deletion not allowed']);
        }
        if (!$this->user->id) {
            return response()->json(['status' => false, 'message' => 'user not found']);
        }
        $result = false;
        $file_root = $this->getS3FilePath();
        $document = Document::query()
            ->where('user_id', $this->user->id)
            ->where('s3root', $file_root)
            ->first();
        if ($document) {
            //$this->storage->delete($file_root); //physically deleting a file on the server
            $result['status'] = $document->delete();
            $message = 'document moved to trash';
        }

        return response()->json(
            ['result' => $result, 'message' => $message ?? 'document not found', 'user_id' => $this->user->id]
        );
    }

    private function getS3FilePath(): string
    {
        return $this->getRequestDir() . '/' . $this->request->get(
                self::$get_param_filename
            ) . self::$encryption_extension;
    }

    public function unlinkUserDir(StoreRequest $request): JsonResponse
    {
        $this->request = $request;
        $this->user = $this->getUser($request);
        if (!self::ENABLE_REMOVE) {
            return response()->json(['status' => false, 'message' => 'deletion not allowed']);
        }
        if (!$this->user->id) {
            return response()->json(['status' => false, 'message' => 'user not found']);
        }
        $like = $this->getRequestDir();
        $files = Document::query()
            ->where('user_id', $this->user->id)
            ->where('s3root', 'like', "$like/%")
            ->get();
        foreach ($files as $file) {
            $document = Document::query()
                ->where('id', $file->id)
                ->first();
            $document?->delete();
        }

        return response()->json(['status' => $files->count() > 0, 'message' => 'deleted ' . $files->count() . ' files']
        );
    }

    public function getUserDirNamesByDir(StoreRequest $request): JsonResponse
    {
        $this->request = $request;
        $this->user = $this->getUser($request);
        if (!$this->user->id) {
            return response()->json(['status' => false, 'message' => 'user not found']);
        }
        $result = $this->user->getUserFolders($this->getRequestDir(), $this->request->get('recursive'));
        return response()->json(
            ['data' => ['location' => ltrim($this->getRequestDir() . '/', $this->user->id), 'folders' => $result]]
        );
    }

    public function downloadFile(StoreRequest $request): JsonResponse|StreamedResponse
    {
        $this->request = $request;
        $this->user = $this->getUser($request);
        $this->document = $this->fileExist();
        if (!$this->document && $this->folderExist() && !$this->request->get(self::$get_param_filename)) {
            return $this->downloadArchiveFolder();
        }
        if (!$this->document) {
            return response()->json(['status' => false, 'message' => 'file not found']);
        }
        if ($this->document->status !== self::UPLOAD_STATUS) {
            return response()->json(['status' => false, 'message' => "file status is {$this->document->status}"]);
        }
        if ((!$this->document->is_remote && env('STORAGE_TYPE') === 's3') ||
            ($this->document->is_remote && env('STORAGE_TYPE') !== 's3')) {
            return response()->json(['status' => false, 'description' => 'File in another storage type.']);
        }
        if (!$this->request->get('download')) {
            $content_type = $this->document->content_type;
            header("Content-Type: $content_type");
            FileVault::disk(env('STORAGE_TYPE'))
                ->streamDecrypt($this->getS3FilePath());
            die();
        }
        return response()->streamDownload(function () {
            FileVault::disk(env('STORAGE_TYPE'))
                ->streamDecrypt($this->getS3FilePath());
        }, $this->request->get(self::$get_param_filename));
    }

    private function fileExist($checkName = null): Model|HasMany|null
    {
        $checkName = $checkName ?? $this->request->get(self::$get_param_filename);
        $file_source = $this->getRequestDir() . '/' . $checkName . self::$encryption_extension;
        return $this->user->files()->withTrashed()->firstWhere('s3root', $file_source);
    }

    private function folderExist(): bool
    {
        return $this->user->checkFolderExist($this->getRequestDir());
    }

    private function downloadArchiveFolder(): StreamedResponse
    {
        $archive = new ArchiveOptions();
        $archive->setContentType('application/octet-stream');
        $archive->setZeroHeader(true);
        $archive->setFlushOutput(true);
        $archive->setSendHttpHeaders(true);

        $filename = pathinfo($this->getRequestDir(), PATHINFO_FILENAME) . '.zip';
        $zip = new ZipStream\ZipStream($filename, $archive);
        $objects = $this->storage->allFiles($this->getRequestDir());
        return new StreamedResponse(function () use ($objects, $zip) {
            foreach ($objects as $object) {
                if (!$document = Document::where('s3root', $object)->first()) {
                    continue;
                }
                $tmp_file = Str::endsWith($object, self::$encryption_extension) ? Str::replaceLast(
                    self::$encryption_extension,
                    '',
                    $object
                ) : $object;
                if ($document->is_remote) {
                    Storage::put($object, $this->storage->get($object));
                }
                if ($object !== $tmp_file) {
                    FileVault::decrypt($object, $tmp_file, $document->is_remote);
                }
                $zip->addFile($this->user->stripPathPrefix($tmp_file, $this->getRequestDir()), Storage::get($tmp_file));
                Storage::delete($tmp_file);
            }
            $zip->finish();
        });
    }

    public function store(StoreRequest $request): JsonResponse
    {
        $this->request = $request;
        $this->user = $this->getUser($request);
        $files = $request->file('userFile');
        $result = [];
        $count = 0;
        $countRequestFiles = count($request->file('userFile'));
        $serverLimitFiles = ini_get('max_file_uploads');
        if ($countRequestFiles == $serverLimitFiles) {
            return response()->json(
                [
                    'status' => false,
                    'file_count' => 0,
                    'message' => 'max limit files to upload is '.$serverLimitFiles - 1,
                ]
            );
        }
        if ($this->request->hasFile('userFile')) {
            foreach ($files as $file) {
                $getClientOriginalName = $file->getClientOriginalName();
                $this->uploadClientOriginalNameFile = $getClientOriginalName;
                $this->getNameIfExist($getClientOriginalName);
                $filename = Storage::putFileAs($this->getRequestDir(), $file, $this->writeFilename);
                if ($filename) {
                    ++$count;
                    $document = new Document();
                    $document->filename = $this->writeFilename;
                    $document->s3root = $filename . self::$encryption_extension;
                    $document->is_remote = env('STORAGE_TYPE') === 's3';
                    $document->user_id = $this->user->id;
                    $document->size = $file->getSize();
                    $document->doctype = $this->request->get('doctype');
                    $document->content_type = $file->getMimeType();
                    $document->save();
                    EncryptFile::withChain([
                        new MoveFileToS3($filename, $document->id)
                    ])->dispatch($filename);
                }
                $result[] = [
                    'message' => 'File successfully uploaded',
                    'file' => $this->writeFilename,
                    'uploaded' => true,
                ];
            }
        }
        return response()->json(
            [
                'status' => true,
                'file_count' => $count,
                'location' => ltrim($this->getRequestDir() . '/', $this->user->id),
                'data' => $result
            ]
        );
    }

    private function getNameIfExist(string $uploadClientOriginalNameFile, $i = 0): string
    {
        ++$i;
        if ($this->fileExist($uploadClientOriginalNameFile) || Storage::exists(
                $this->getRequestDir() . '/' . $uploadClientOriginalNameFile
            )) {
            $uploadClientOriginalNameFile = pathinfo(
                    $this->uploadClientOriginalNameFile,
                    PATHINFO_FILENAME
                ) . '(' . $i . ').' . pathinfo($this->uploadClientOriginalNameFile)['extension'];
            return $this->getNameIfExist($uploadClientOriginalNameFile, $i);
        }
        $this->writeFilename = $uploadClientOriginalNameFile;
        $this->writeFilenameToLog[] = $uploadClientOriginalNameFile;
        return $this->uploadClientOriginalNameFile = $uploadClientOriginalNameFile;
    }
}
